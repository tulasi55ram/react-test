/**
 * This is an example request. Create your own using best practises for
 * handling asynchronous data fetching
 **/
const fetch = require("node-fetch");
const Bluebird = require("bluebird");
fetch.Promise = Bluebird;

const headers = {
  "Content-Type": "application/json",
  "Access-Control-Origin": "*"
};

export const getData = apiUrl => {
  try {
    return new Promise((resolve, reject) => {
      fetch(apiUrl, {
        method: "GET",
        headers: headers
      })
        .then(res => {
          return res.json();
        })
        .then(data => {
          return resolve(data);
        })
        .catch(e => {
          let exception = { status: 400, msg: e.toString() };
          return resolve(exception);
        });
    });
  } catch (e) {
    console.log(e);
  }
};

/*export const getData = cb => {
  const vehicles = new XMLHttpRequest();
  vehicles.open("GET", "http://localhost:9988/api/vehicle");

  vehicles.onreadystatechange = function() {
    if (vehicles.readyState === 4) {
      if (vehicles.status === 200) {
        cb(vehicles.responseText);
      }
    }
  };
 
  vehicles.send();
  
};
*/
