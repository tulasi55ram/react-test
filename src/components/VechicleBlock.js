import React, { Component } from "react";
import { DOMAIN_NAME } from "../constants";
export default class VechicleBlock extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { data } = this.props;
    const media = data.media; 
    const imageUrl = DOMAIN_NAME + media[0].url;
    const name = media[0].name;
    const otherData = data.otherData;

    return (
      <div className="col-lg-3 col-md-6 col-sm-12 p-0 text-md-center text-sm-left vehicle-block">
        <div className="vechicle-details">
          <div className="vehicle-featured-image">
            <img className="vehicle-image" src={imageUrl} />
          </div>
          <div className="other-information">
            <h3 className="vehicle-name uppercase">{name}</h3>
            <div className="price">{otherData.price}</div>
            <div className="desc">{otherData.description}</div>
          </div>
        </div>
      </div>
    );
  }
}
