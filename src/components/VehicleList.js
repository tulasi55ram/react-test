import React, { Component } from "react";
import { DOMAIN_NAME } from "../constants";
import { getData } from "../api";
import VechicleBlock from "../components/VechicleBlock";


const GET_ALL_VEHICLES = DOMAIN_NAME + "/api/vehicle";

export default class VehicleList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null
    };
  }

  async componentDidMount() {
    try {
      let response = await getData(GET_ALL_VEHICLES);
      let vehiclesList = response.vehicles;
      if (vehiclesList && vehiclesList instanceof Array) {
        const promises = vehiclesList.map(obj => {
          return getData(DOMAIN_NAME + obj.url).then(res => {
            obj.otherData = res;
            return obj;
          });
        });
        let data = await Promise.all(promises);
        this.setState({ data });
      }
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { data } = this.state;
    console.log(data);
    if (data) {
      return (
        <div className="row">
          {data.map((obj, index) => (
            <VechicleBlock data={obj} key={index} />
        ))}
        </div>
      );
    }

    return <h1>Loading...</h1>;
  }
}
